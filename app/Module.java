import Resolvers.DefaultRedisInstance;
import Resolvers.NamedRedisInstance;
import com.google.inject.AbstractModule;

import com.google.inject.Inject;
import com.google.inject.Provides;
import com.google.inject.name.Names;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import play.api.cache.redis.configuration.RedisInstance;
import play.cache.NamedCache;

import javax.inject.Named;

/**
 * This class is a Guice module that tells Guice how to bind several
 * different types. This Guice module is created when the Play
 * application starts.
 *
 * Play will automatically use any class called `Module` that is in
 * the root package. You can create modules in other locations by
 * adding `play.modules.enabled` settings to the `application.conf`
 * configuration file.
 */
public class Module extends AbstractModule {

    @Inject
    Config configuration;

    private boolean isDCOSEnabled;
    private String redisHost;

    @Override
    public void configure() {
        configuration = ConfigFactory.load();
        isDCOSEnabled = configuration.getBoolean("DCOS.enabled");

        redisHost = isDCOSEnabled ? "redis.marathon.l4lb.thisdcos.directory" : "localhost";
        //bind(RedisInstance.class).annotatedWith(Names.named("play")).to(DefaultRedisInstance.class);
    }

    @Provides
    @NamedCache("redis")
    public RedisInstance getRedisCacheInstance()
    {
        return new NamedRedisInstance("redis", redisHost, 6379, 0, null);
    }


    @Provides
    @NamedCache("play")
    public RedisInstance getPlayCacheInstance()
    {
        return new NamedRedisInstance("play", redisHost, 6379, 0, null);
    }

    @Provides
    @NamedCache("ITS")
    public RedisInstance getITSCacheInstance()
    {
        return new NamedRedisInstance("ITS", redisHost, 6379, 2, null);
    }
}