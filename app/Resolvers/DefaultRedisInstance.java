package Resolvers;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import play.api.cache.redis.configuration.RedisStandalone;
import scala.Option;
import scala.concurrent.duration.FiniteDuration;

import javax.inject.Inject;
import java.util.concurrent.TimeUnit;

public class DefaultRedisInstance implements RedisStandalone {
    private String name;
    private String prefix;
    private String host;
    private int port;
    private int db;
    private String password;

    private static final String type = "standalone";
    private static final String dispatcher = "akka.actor.default-dispatcher";
    private static final String recovery = "log-and-default";


    @Inject
    public DefaultRedisInstance(Config configuration)
    {
        configuration = ConfigFactory.load();
        boolean isDCOSEnabled = configuration.getBoolean("DCOS.enabled");

        String redisHost = isDCOSEnabled ? "redis.marathon.l4lb.thisdcos.directory" : "localhost";

        this.name = "play";
        this.prefix = null; // Change in future when required.
        this.db = 0;

        this.host = redisHost;
        this.port = 6379;

        this.password = "";
    }

    @Override
    public String name() {
        return this.name;
    }

    @Override
    public String host() {
        return this.host;
    }

    @Override
    public int port() {
        return this.port;
    }

    @Override
    public Option<Object> database() { return Option.apply(this.db); }

    @Override
    public Option<String> password() { return Option.apply(this.password); }

    @Override
    public String invocationContext() { return this.dispatcher; }

    @Override
    public String invocationPolicy() { return "lazy"; }

    @Override
    public Option<String> prefix() {
        return Option.apply(this.prefix);
    }

    @Override
    public String source() {return this.type; }

    @Override
    public String recovery() {
        return this.recovery;
    }

    @Override
    public FiniteDuration timeout() {
        return new FiniteDuration(1, TimeUnit.SECONDS);
    }
}
