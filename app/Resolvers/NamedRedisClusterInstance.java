package Resolvers;

import play.api.cache.redis.configuration.RedisCluster;
import play.api.cache.redis.configuration.RedisHost;
import scala.Option;
import scala.concurrent.duration.FiniteDuration;

import java.util.List;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

public class NamedRedisClusterInstance implements RedisCluster {

    private String name;
    private String prefix;
    private String host;
    private int port;
    private int db;
    private String password;


    private static final String type = "cluster";
    private static final String dispatcher = "akka.actor.default-dispatcher";
    private static final String recovery = "log-and-default";

    public NamedRedisClusterInstance(String name, String host, int port, int db, String password)
    {
        this.name = name;
        this.prefix = null;
        this.host = host;
        this.port = port;
        this.db = db;
        this.password = password;
    }

    @Override
    public String source() { return this.type; }

    @Override
    public String name() { return this.name; }

    @Override
    public Option<String> prefix() { return Option.apply(this.prefix); }

    @Override
    public String recovery() { return this.recovery; }

    @Override
    public FiniteDuration timeout() { return new FiniteDuration(1, TimeUnit.SECONDS); }

    @Override
    public scala.collection.immutable.List<RedisHost> nodes() {
        List<RedisHost> hostList = new ArrayList<>();
        hostList.add(getHost(30001));
        hostList.add(getHost(30002));
        hostList.add(getHost(30003));
        hostList.add(getHost(30004));
        hostList.add(getHost(30005));
        hostList.add(getHost(30006));
        return scala.collection.JavaConverters.collectionAsScalaIterableConverter(hostList).asScala().toList();
    }

    @Override
    public String invocationContext() { return this.dispatcher; }
    
    @Override
    public String invocationPolicy() { return "lazy"; }

    private RedisHost getHost(int port) {
        return new NamedRedisHost(this.host, port, this.db, this.password);
    }
}
