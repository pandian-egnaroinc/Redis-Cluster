package Resolvers;

import play.api.cache.redis.configuration.RedisStandalone;
import scala.Option;
import scala.concurrent.duration.FiniteDuration;

import java.util.concurrent.TimeUnit;

public class NamedRedisInstance implements RedisStandalone {
    private String name;
    private String prefix;
    private String host;
    private int port;
    private int db;
    private String password;

    private static final String type = "standalone";
    private static final String dispatcher = "akka.actor.default-dispatcher";
    private static final String recovery = "log-and-default";

    public NamedRedisInstance(String name, String host, int port, int db, String password)
    {
        this.name = name;
        this.prefix = name; // Change in future when required.
        this.db = db;

        this.host = host;
        this.port = port;

        this.password = password;
    }

    @Override
    public String name() {
        return this.name;
    }

    @Override
    public String host()
    {
        return this.host;
    }

    @Override
    public int port()
    {
        return this.port;
    }

    @Override
    public Option<Object> database() { return Option.apply(this.db); }

    @Override
    public Option<String> password() { return Option.apply(this.password); }

    @Override
    public String invocationContext() { return this.dispatcher; }

    @Override
    public String invocationPolicy() { return "lazy"; }

    @Override
    public Option<String> prefix() {
        return Option.apply(this.prefix);
    }

    @Override
    public String source() {return this.type; }

    @Override
    public String recovery() {
        return this.recovery;
    }

    @Override
    public FiniteDuration timeout() {
        return new FiniteDuration(1, TimeUnit.SECONDS);
    }
}
