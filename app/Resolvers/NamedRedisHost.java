package Resolvers;

import play.api.cache.redis.configuration.RedisHost;
import scala.Option;

public class NamedRedisHost implements RedisHost {
    private String host;
    private int port;
    private int db;
    private String password;

    public NamedRedisHost(String host, int port, int db, String password)
    {
        this.host = host;
        this.port = port;
        this.db = db;
        this.password = password;
    }

    @Override
    public String host() {
        return this.host;
    }

    @Override
    public int port() {
        return this.port;
    }

    @Override
    public Option<Object> database() {
        return Option.apply(this.db);
    }

    @Override
    public Option<String> password() {
        return Option.apply(this.password);
    }
}
