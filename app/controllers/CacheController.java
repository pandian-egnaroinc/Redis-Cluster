package controllers;

import play.cache.NamedCache;
import play.cache.SyncCacheApi;
import play.mvc.Controller;
import play.mvc.Result;

import javax.inject.Inject;
import com.google.gson.Gson;

public class CacheController extends Controller {
    private SyncCacheApi redisCache;
    private SyncCacheApi itsCache;
    private SyncCacheApi defaultCache;
    private Gson gson;

    @Inject
    public CacheController(@NamedCache("redis") SyncCacheApi redisCache,
                           @NamedCache("ITS") SyncCacheApi itsCache,
                           SyncCacheApi defaultCache) {
        this.redisCache = redisCache;
        this.itsCache = itsCache;
        this.defaultCache = defaultCache;
        this.gson = new Gson();
    }

    public Result addToCache(String key, String value)
    {
        try {
            this.itsCache.set(key, value);
        }
        catch (Exception e) {
            System.out.println("Exception encountered while adding : " + e.getMessage());
            e.printStackTrace();
            return ok("Exception encountered while adding : " + e.getMessage());
        }
        return ok("Added key : " + key + " with value : " + value + " to the cache");
    }

    public Result getFromCache(String key)
    {
        Object value = this.itsCache.get(key);

        if(null == value)
            return ok("Value not found in cache for key : " + key);

        return ok("key : " + key + " value : " + value.toString());
    }
}
