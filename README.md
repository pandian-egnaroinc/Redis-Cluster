# Redis-Demo

Demo for cache operations using play-redis.

## Running

Run this using [sbt](http://www.scala-sbt.org/).  If you downloaded this project from http://www.playframework.com/download then you'll find a prepackaged version of sbt in the project directory:

```
sbt run
```

And then go to http://localhost:9000 to see the running web application.

## Controllers

- CacheController.java:

  Shows how to handle simple HTTP requests for adding and removing from redis cache.


## Usage

  http://localhost:9000/addToCache/key1/value1
        - Adds key1 with value1 to cache.

  http://localhost:9000/getFromCache/key1
        - Retrieves the value of key1 (value1) from cache.

## Components

- Module.java:

  Binds various types of cache configurations.