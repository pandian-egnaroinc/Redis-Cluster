name := "Redis-Cluster"
packageName in Docker := "pandianegnaro/dcos-redis"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.12.4"

crossScalaVersions := Seq("2.11.12", "2.12.4")

libraryDependencies += guice
libraryDependencies += cacheApi

// Test Database
libraryDependencies += "com.h2database" % "h2" % "1.4.196"

// Testing libraries for dealing with CompletionStage...
libraryDependencies += "org.assertj" % "assertj-core" % "3.6.2" % Test
libraryDependencies += "org.awaitility" % "awaitility" % "2.0.0" % Test

// enable Play cache API (based on your Play version)
libraryDependencies += play.sbt.PlayImport.cacheApi

// include play-redis library
//libraryDependencies += "com.github.karelcemus" %% "play-redis" % "2.0.2"

// Gson.
libraryDependencies += "com.google.code.gson" % "gson" % "2.8.2"

// Make verbose tests
testOptions in Test := Seq(Tests.Argument(TestFrameworks.JUnit, "-a", "-v"))

// https://mvnrepository.com/artifact/org.scala-stm/scala-stm
libraryDependencies += "org.scala-stm" %% "scala-stm" % "0.8"

